<?php

class WP_Plugin {
	
	public $plugin_slug;
	public $menus;
	public $use_plugin_path;
	
	// ACTIONS & FILTERS & HOOKS
	const ADD_MENU_ACTION = 'admin_menu';
	
	
	public function __construct( $plugin_slug = '' ) {
		if( empty( $plugin_slug ) ){
			$this->plugin_slug = 'plugin-' . time();
		} else {
			$this->plugin_slug = $plugin_slug;
		}
		
		$this->use_plugin_path = false;
	}
	
	// MENUS
	
	function newAdminMenu( $menu_name ){
		$new_menu = new WP_Plugin_Menu( $menu_name );
		$menus[] = $new_menu;
		
		$this->postMethod();
		
		return $new_menu;
	}
	
	// PATHS
	
	public static function getPluginPath(){
		// Q: will this work from anywhere, or only from .php files in the plugin dir?
		return plugin_dir_path( __FILE__ );
	}
	
	public static function getPathToPluginFile( $file_path ){
		return WP_Plugin::getPluginPath() . $file_path;
	}
	
	// UTIL FUNCTIONS
	
	function usePluginPathOnce(){
		$this->use_plugin_path = true;
		
		return $this;
	}
	
	// GENERATE STUFF
	
	function genSettingsPage( $file_path ){
		$file_path_final = $this->use_plugin_path ? WP_Plugin::getPathToPluginFile( $file_path ) : $file_path;
		
		$this->postMethod();
	}
	
	function generateCode(){
		// generate raw Wordpress PHP code for everything declared
		//// e.g., add_menu_page and add_submenu_page for menu operations
		//// so you're not tied to this wrapper class
	}
	
	// EVENT FUNCTIONS
	
	/// called after every chainable method
	function postMethod(){
		$this->use_plugin_path = false; // reset after usePluginPathOnce()
	}
	
}

class WP_Plugin_Menu {
	
	public $name;
	public $slug;
	public $submenu_slugs; // [ 'menu_name' => 'slug, ... ]
	public $use_plugin_path;
	
	public $last_return;
	
	public function __construct( $name = '' ){
		$this->name = $name;
		
		if( empty( $name ) ){
			$this->slug = 'menu-' . time();
		} else {
			$this->slug = sanitize_title( $name );
		}
		
		// default property values
		$this->use_plugin_path = false;
	}
	
	function addPrimaryMenuItem( $submenu_name, $page_title, $file_path, $capability = 'manage_options' ){
		$menu_name = $this->name;
		$menu_slug = $this->slug;
		
		$file_path_final = $this->use_plugin_path ? WP_Plugin::getPathToPluginFile( $file_path ) : $file_path;
		
		$callback_fn = function() use( $file_path_final ) {
			include( $file_path_final );
		};
		
		add_menu_page( $page_title, $menu_name, $capability, $menu_slug, $callback_fn );
		add_submenu_page( $menu_slug, $page_title, $submenu_name, $capability, $menu_slug, $callback_fn );
		
		return $this;
	}
	
	function addMenuItem( $submenu_name, $page_title, $file_path, $capability = 'manage_options' ){
		// TODO: Account for DUPLICATE MENU NAMES (currently doesn't support duplicates, only overrides)
		$parent_slug = $this->slug;
		$submenu_slug = sanitize_title( "$parent_slug-$submenu_name" );
		
		$file_path_final = $this->use_plugin_path ? WP_Plugin::getPathToPluginFile( $file_path ) : $file_path;
		
		$callback_fn = function() use( $file_path_final ) {
			include( $file_path_final );
		};
		
		add_submenu_page( $parent_slug, $page_title, $submenu_name, $capability, $submenu_slug, $callback_fn );
		
		$submenu_slugs[ $submenu_name ] = $submenu_slug;
		
		return $this;
	}
	
	function removeMenuItem( $submenu_name ){
		// TODO: case insensitivize input!
		if( !array_key_exists( $submenu_name, $this->submenu_slugs ) ) return false;
		
		$submenu_slug = $this->submenu_slugs[ $submenu_name ];
		$menu_slug = $this->slug;
		
		$this->last_return = remove_submenu_page( $menu_slug, $submenu_slug );
		
		return $this;
	}
	
	// UTIL FUNCTIONS
	
	function usePluginPath(){
		$this->use_plugin_path = true;
		
		return $this;
	}
	
}

?>