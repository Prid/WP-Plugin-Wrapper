<?php
/**
* Plugin Name: WP Plugin Wrapper
* Plugin URI: 
* Description: Wrapper for common WP Plugin Development operations
* Version: 0.3
* Author: Prid
**/

include_once 'wp_plugin.php';


$wpp = new WP_Plugin();

$wpp->usePluginPathOnce()->genSettingsPage( 'admin_settings_page.php' );

add_action( WP_Plugin::ADD_MENU_ACTION, 'wp_plugin_setup_admin_menu' );

function wp_plugin_setup_admin_menu(){
	global $wpp;
	
	$menu = $wpp->newAdminMenu( 'Tekiyaki' );
	$menu
		->usePluginPath() // ALTS: WP_Plugin::getPathToPluginFile( 'admin_page.php' ) OR WP_Plugin::getPluginPath() . 'admin_vars_page.php' )
		->addPrimaryMenuItem( 'Import', 'Content Plugin - Import', 'admin_page.php' )
		->addMenuItem( 'Variables', 'Content Plugin - Variables', 'admin_vars_page.php' )
		->addMenuItem( 'Settings', 'Content Plugin - Settings', 'admin_settings_page.php' );

	// $menu->removeMenuItem( 'Import' );
	// $removedItem = $menu->last_return;
	
}

?>